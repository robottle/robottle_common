package connection

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/x/bsonx"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	cfg "gitlab.com/robottle/robottle_common/config"
)

const (
	AscendingIndexType  IndexType = "ascending"
	DescendingIndexType IndexType = "descending"
)

type IndexType string

type CollectionIndex struct {
	Collection string
	Name       string
	Keys       []string
	Order      IndexType
	Unique     bool
}

func NewMongoDBConnection(name string) (database *mongo.Database, err error) {
	config, err := cfg.GetConfig()
	if err != nil {
		return nil, err
	}
	settings, found := config.Database[name]
	if !found {
		return nil, fmt.Errorf("database configuration for %s not found", name)
	}
	var indexes map[interface{}]interface{}
	value, found := settings.Params["indexes"]
	if found {
		indexes = value.(map[interface{}]interface{})
		delete(settings.Params, "indexes")
	}
	url := settings.URL(true, false)
	settings.Params["indexes"] = value
	log.Printf("url: %s", url)
	opts := options.
		Client().
		ApplyURI(url).
		SetReplicaSet("rs0").
		//SetServerSelectionTimeout(30 * time.Second).
		//SetHosts([]string{fmt.Sprintf("%s:%d", settings.Hostname, settings.Port)}).
		SetAuth(options.Credential{
			Username:   settings.Username,
			Password:   settings.Password,
			AuthSource: "admin",
		})
	client, err := mongo.NewClient(opts)
	if err != nil {
		return nil, err
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}
	database = client.Database(settings.Database)
	if len(indexes) > 0 {
		collectionIndexes := make([]CollectionIndex, 0)
		for name, values := range indexes {
			collectionIndexValues := values.(map[interface{}]interface{})
			keys := make([]string, len(collectionIndexValues["keys"].([]interface{})))
			for i, key := range collectionIndexValues["keys"].([]interface{}) {
				keys[i] = key.(string)
			}
			collectionIndex := CollectionIndex{
				Name:       name.(string),
				Collection: collectionIndexValues["collection"].(string),
				Keys:       keys,
				Order:      IndexType(collectionIndexValues["order"].(string)),
				Unique:     collectionIndexValues["unique"].(bool),
			}
			collectionIndexes = append(collectionIndexes, collectionIndex)
		}
		if err := CreateIndexes(database, collectionIndexes...); err != nil {
			return nil, err
		}
	}
	return database, err
}

func CreateIndexes(database *mongo.Database, collectionIndexes ...CollectionIndex) (err error) {
	ctx := context.Background()
	for _, collectionIndex := range collectionIndexes {
		opts := options.CreateIndexes()
		keys := bsonx.Doc{}
		order := int32(-1)
		if collectionIndex.Order == AscendingIndexType {
			order = 1
		}
		for _, key := range collectionIndex.Keys {
			elem := bsonx.Elem{Key: key, Value: bsonx.Int32(order)}
			keys = append(keys, elem)
		}
		background := true
		indexOptions := &options.IndexOptions{
			Name:       &collectionIndex.Name,
			Unique:     &collectionIndex.Unique,
			Background: &background,
		}
		index := mongo.IndexModel{
			Keys:    keys,
			Options: indexOptions,
		}
		if _, err = database.Collection(collectionIndex.Collection).Indexes().CreateOne(ctx, index, opts); err != nil {
			return err
		}
		log.Printf(
			"Added index %s on collection %s (%s)",
			collectionIndex.Name,
			collectionIndex.Collection,
			database.Name(),
		)
	}
	return
}
