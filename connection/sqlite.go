package connection

import (
	"github.com/jinzhu/gorm"
	// Dialect for SQLite
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"gitlab.com/robottle/robottle_common/config"
)

func getSQLiteConnection(settings *config.Database) (db *gorm.DB, err error) {
	path := settings.URL(false, false)
	db, err = gorm.Open(string(settings.Adapter), path)
	return
}
