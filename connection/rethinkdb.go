package connection

import (
	"fmt"

	r "gopkg.in/rethinkdb/rethinkdb-go.v5"

	cfg "gitlab.com/robottle/robottle_common/config"
)

type TableIndex struct {
	Table string
	Field string
}

func NewRethinkDBConnection(name string) (session *r.Session, err error) {
	config, err := cfg.GetConfig()
	settings, found := config.Database[name]
	if !found {
		return nil, fmt.Errorf("database configuration settings for %s not found", name)
	}
	var indexes map[interface{}]interface{}
	value, found := settings.Params["indexes"]
	if found {
		indexes = value.(map[interface{}]interface{})
		delete(settings.Params, "indexes")
	}
	// url := settings.URL(false, false)
	settings.Params["indexes"] = indexes
	session, err = r.Connect(r.ConnectOpts{
		Address:    fmt.Sprintf("%s:%d", settings.Hostname, settings.Port),
		InitialCap: 10,
		MaxOpen:    10,
		Database:   settings.Database,
		Username:   settings.Username,
		Password:   settings.Password,
	})
	if err != nil {
		return nil, err
	}
	if err := createDatabase(session, settings.Database); err != nil {
		return nil, err
	}
	if tableDefinitions, found := settings.Params["tables"]; found {
		tables := make([]string, 0)
		for _, table := range tableDefinitions.([]interface{}) {
			tables = append(tables, table.(string))
		}
		if err := createTables(session, settings.Database, tables...); err != nil {
			return nil, err
		}
	}
	if indexDefinitions, found := settings.Params["indexes"]; found {
		tableIndexes := make([]*TableIndex, 0)
		for _, index := range indexDefinitions.(map[interface{}]interface{}) {
			index := index.(map[interface{}]interface{})
			tableIndex := &TableIndex{
				Table: index["table"].(string),
				Field: index["field"].(string),
			}
			tableIndexes = append(tableIndexes, tableIndex)
		}
		if err := createIndexes(session, settings.Database, tableIndexes); err != nil {
			return nil, err
		}
	}
	session.Use(settings.Database)
	return
}

func createIndexes(session *r.Session, database string, indexes []*TableIndex) error {
	for _, tableIndex := range indexes {
		cursor, err := r.DB(database).Table(tableIndex.Table).IndexList().Run(session)
		if err != nil {
			return err
		}
		existingIndexes := make([]string, 0)
		if err := cursor.All(&existingIndexes); err != nil {
			return err
		}
		if !existsInCollection(existingIndexes, tableIndex.Field) {
			if err := r.DB(database).Table(tableIndex.Table).IndexCreate(tableIndex.Field).Exec(session); err != nil {
				return err
			}
		}
	}
	return nil
}

func createTables(session *r.Session, database string, tables ...string) error {
	cursor, err := r.DB(database).TableList().Run(session)
	if err != nil {
		return err
	}
	existingTables := make([]string, 0)
	if err := cursor.All(&existingTables); err != nil {
		return err
	}
	for _, table := range tables {
		if !existsInCollection(existingTables, table) {
			if err := r.DB(database).TableCreate(table).Exec(session); err != nil {
				return err
			}
		}
	}
	return nil
}

func createDatabase(session *r.Session, database string) error {
	return r.DBList().Do(func(result r.Term) r.Term {
		return r.Branch(
			result.Contains(database),
			nil,
			r.DBCreate(database),
		)
	}).Exec(session)
}

func existsInCollection(collection []string, item string) bool {
	for _, value := range collection {
		if value == item {
			return true
		}
	}
	return false
}
