package connection

import (
	"github.com/jinzhu/gorm"
	// Dialect for MySQL
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"gitlab.com/robottle/robottle_common/config"
)

func getMySQLConnection(settings *config.Database) (db *gorm.DB, err error) {
	url := settings.URL(true, false)
	db, err = gorm.Open(string(settings.Adapter), url)
	return
}
