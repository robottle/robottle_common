package connection

import (
	"github.com/streadway/amqp"

	"gitlab.com/robottle/robottle_common/config"
)

func GetAMQPConnection(settings *config.Messaging) (conn *amqp.Connection, err error) {
	url := settings.URL(true, false)
	vhost := "/"
	if param, found := settings.Params["vhost"]; found {
		vhost = param.(string)
	}
	conn, err = amqp.DialConfig(url, amqp.Config{
		Vhost: vhost,
	})
	return
}
