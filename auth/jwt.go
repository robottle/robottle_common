package auth

import (
	"crypto/rsa"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/robottle/robottle_common/config"
)

// JWTAuthStrategy is an authentication strategy that uses JWT.
type JWTAuthStrategy struct {
	issuer     string
	audience   string
	expiration time.Duration
	privateKey *rsa.PrivateKey
	publicKey  *rsa.PublicKey
}

// NewJWTAuthStrategy returns a instance of `JWTAuthStrategy`.
func NewJWTAuthStrategy(settings *config.Auth) (Strategy, error) {
	key, err := ioutil.ReadFile(settings.PrivateKey)
	if err != nil {
		return nil, fmt.Errorf("error reading JWT private key: %v", err)
	}
	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(key)
	if err != nil {
		return nil, fmt.Errorf("error parsing JWT private key: %v", err)
	}

	key, err = ioutil.ReadFile(settings.PublicKey)
	if err != nil {
		return nil, fmt.Errorf("error reading JWT public key: %v", err)
	}
	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(key)
	if err != nil {
		return nil, fmt.Errorf("error parsing JWT public key: %v", err)
	}

	expiration := settings.Expire * time.Hour

	strategy := &JWTAuthStrategy{
		issuer:     settings.Issuer,
		audience:   settings.Audience,
		privateKey: privateKey,
		publicKey:  publicKey,
		expiration: expiration,
	}

	return strategy, nil
}

// GenerateToken using JWT.
func (s *JWTAuthStrategy) GenerateToken(data map[string]interface{}) (string, error) {
	token := jwt.New(jwt.SigningMethodRS256)

	now := time.Now()
	claims := jwt.MapClaims{
		"aud": s.audience,
		"exp": now.Add(s.expiration).Unix(),
		"iss": s.issuer,
		"iat": now,
	}
	for key, value := range data {
		claims[key] = value
	}
	token.Claims = claims

	tokenString, err := token.SignedString(s.privateKey)
	if err != nil {
		return "", fmt.Errorf("error signing token: %v", err)
	}
	return tokenString, nil
}

// VerifyToken checks if the given `tokenString` is a valid JWT token.
func (s *JWTAuthStrategy) VerifyToken(tokenString string) (map[string]interface{}, error) {
	token, err := jwt.Parse(tokenString, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("invalidsigning method")
		}
		return s.publicKey, nil
	})
	if err != nil {
		return nil, fmt.Errorf("invalid token: %v", err)
	}
	return token.Claims.(jwt.MapClaims), nil
}
