package auth

import (
	"fmt"

	"gitlab.com/robottle/robottle_common/config"
)

// Strategy is an interface for an authentication strategy.
type Strategy interface {
	// GenerateToken for the given `data`.
	GenerateToken(data map[string]interface{}) (token string, err error)
	// VerifyToken checks if the given `token` is valid.
	VerifyToken(token string) (data map[string]interface{}, err error)
}

// NewAuthStrategy returns a new authentication strategy.a
func NewAuthStrategy(settings *config.Auth) (Strategy, error) {
	switch settings.Strategy {
	case config.JWTAuthStrategy:
		return NewJWTAuthStrategy(settings)
	default:
		return nil, fmt.Errorf("invalid authentication strategy: %s", settings.Strategy)
	}
}
