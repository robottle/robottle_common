package config

import (
	"bytes"
	"fmt"
)

const (
	AMQPMessagingAdapter MessagingAdapter = "amqp"
)

type MessagingAdapter string

type Messaging struct {
	Adapter  MessagingAdapter `default:"amqp"`
	Hostname string           `default:"rabbitmq"`
	Username string           `default:"guest"`
	Password string           `default:"guest"`
	Port     int              `default:"5672"`
	Params   map[string]interface{}
}

func (m *Messaging) URL(withAdapter bool, wrapHost bool) string {
	var buffer bytes.Buffer

	if withAdapter {
		buffer.WriteString(fmt.Sprintf("%s://", m.Adapter))
	}
	buffer.WriteString(m.Username)
	if len(m.Password) > 0 {
		buffer.WriteString(fmt.Sprintf(":%s", m.Password))
	}
	hostAndPort := fmt.Sprintf("%s:%d", m.Hostname, m.Port)
	if wrapHost {
		buffer.WriteString(fmt.Sprintf("@tcp(%s)", hostAndPort))
	} else {
		buffer.WriteString(fmt.Sprintf("@%s", hostAndPort))
	}
	return buffer.String()
}
