package config

const (
	// BCryptHashingStrategy uses BCrypt.
	BCryptHashingStrategy HashingStrategy = "bcrypt"
	// Argon2HashingStrategy uses Argon2.
	Argon2HashingStrategy HashingStrategy = "argon2"
)

// HashingStrategy defines a hashing strategy.
type HashingStrategy string

// Hashing contains configuration for a hashing strategy.
type Hashing struct {
	Strategy    HashingStrategy `default:"argon2"`
	Memory      uint32          `default:"65536"`
	Iterations  uint32          `default:"3"`
	Parallelism uint8           `default:"2"`
	SaltLength  uint32          `default:"16"`
	KeyLength   uint32          `default:"32"`
	Cost        int             `default:"10"`
}
