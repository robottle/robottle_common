package config

import "strings"

type Service struct {
	Name      string
	Namespace string `default:"io.coderoso.robottle.svc"`
	Version   string `default:"0.0.1"`
}

func (s *Service) URL() (url string) {
	parts := []string{
		s.Namespace, s.Name,
	}
	url = strings.Join(parts, ".")
	return
}
