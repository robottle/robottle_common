package config

type Consumer struct {
	Queue      string
	Exchange   string
	RoutingKey string `yaml:"routing_key"`
	Tag        string
}
