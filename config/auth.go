package config

import "time"

const (
	// JWTAuthStrategy uses JWT.
	JWTAuthStrategy AuthStrategy = "jwt"
)

// AuthStrategy defines an authentication strategy.
type AuthStrategy string

// Auth is the configuration for an authentication strategy.
type Auth struct {
	Strategy       AuthStrategy  `default:"jwt"`
	Issuer         string        `default:"coderoso.io"`
	Audience       string        `default:"coderoso.io"`
	Expire         time.Duration `default:"72"`
	PrivateKey     string        `yaml:"private_key"`
	PublicKey      string        `yaml:"public_key"`
	AllowedMethods []string      `yaml:"allowed_methods"`
}
