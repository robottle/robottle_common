package hashing

import (
	"fmt"

	"gitlab.com/robottle/robottle_common/config"
)

const (
	// MemoryDefault default value for `Params.Memory`.
	MemoryDefault = 64 * 1024
	// IterationsDefault default value for `Params.Iterations`.
	IterationsDefault = 3
	// ParallelismDefault default value for `Params.Parallelism`.
	ParallelismDefault = 2
	// SaltLengthDefault default value for `Params.SaltLength`.
	SaltLengthDefault = 16
	// KeyLengthDefault default value for `Params.KeyLength`.
	KeyLengthDefault = 32
)

// Strategy defines an interface for hashing passwords.
type Strategy interface {
	// GenerateFromPassword generates a new hash from the given `password.`
	GenerateFromPassword(password string) (string, error)
	// ComparePasswordAndHash Compares the given `password` and `encodedHash`.
	ComparePasswordAndHash(password string, encodedHash string) (bool, error)
	// SignHMAC returns an HMAC signature for the given `payload` and `secret`
	SignHMAC(payload []byte, secret []byte) (signature []byte)
	// VerifyHMAC checks if the given `hash` is valid for the given `payload` using the given `secret`.
	ValidateHMAC(payload []byte, hash string, secret string) (err error)
}

// NewHashingStrategy returns an implementation of `Strategy`.
func NewHashingStrategy(settings *config.Hashing) (Strategy, error) {
	switch settings.Strategy {
	case config.BCryptHashingStrategy:
		return NewBCryptHashingStrategy(settings)
	case config.Argon2HashingStrategy:
		return NewArgon2HashingStrategy(settings)
	default:
		return nil, fmt.Errorf("invalid hashing strategy: %s", settings.Strategy)
	}
}
