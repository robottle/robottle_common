package hashing

import (
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/robottle/robottle_common/config"
)

// BCryptHashingStrategy implements `Strategy` using BCrypt.
type BCryptHashingStrategy struct {
	*HMAC

	cost int
}

// NewBCryptHashingStrategy returns an implementation of `Strategy` using BCrypt.
func NewBCryptHashingStrategy(settings *config.Hashing) (Strategy, error) {
	strategy := &BCryptHashingStrategy{
		cost: settings.Cost,
	}
	return strategy, nil
}

// GenerateFromPassword generates a new hash from the given `password.`
func (s *BCryptHashingStrategy) GenerateFromPassword(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), s.cost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

// ComparePasswordAndHash Compares the given `password` and `encodedHash`.
func (s *BCryptHashingStrategy) ComparePasswordAndHash(password string, encodedHash string) (bool, error) {
	err := bcrypt.CompareHashAndPassword([]byte(encodedHash), []byte(password))
	if err != nil {
		return false, err
	}
	return true, nil
}
