package hashing

import (
	"crypto/rand"
	"crypto/subtle"
	"encoding/base64"
	"fmt"
	"strings"

	"golang.org/x/crypto/argon2"

	"gitlab.com/robottle/robottle_common/config"
)

// Argon2HashingParams is a list of parameters for generating a hash.
type Argon2HashingParams struct {
	Memory      uint32
	Iterations  uint32
	Parallelism uint8
	SaltLength  uint32
	KeyLength   uint32
}

// Argon2HashingStrategy is a `Strategy` implementation using Argon2 algorithm.
type Argon2HashingStrategy struct {
	*HMAC

	params *Argon2HashingParams
}

// NewArgon2HashingStrategy returns an implementation of `Strategy`.
func NewArgon2HashingStrategy(settings *config.Hashing) (Strategy, error) {
	params := &Argon2HashingParams{
		Memory:      settings.Memory,
		Iterations:  settings.Iterations,
		Parallelism: settings.Parallelism,
		SaltLength:  settings.SaltLength,
		KeyLength:   settings.KeyLength,
	}
	strategy := &Argon2HashingStrategy{
		params: params,
	}
	return strategy, nil
}

// GenerateFromPassword generates a new hash from the given `password.`
func (s *Argon2HashingStrategy) GenerateFromPassword(password string) (string, error) {
	salt, err := generateRandomBytes(s.params.SaltLength)
	if err != nil {
		return "", err
	}
	hash := argon2.IDKey([]byte(password), salt, s.params.Iterations, s.params.Memory, s.params.Parallelism, s.params.KeyLength)

	b64Salt := base64.RawStdEncoding.EncodeToString(salt)
	b64Hash := base64.RawStdEncoding.EncodeToString(hash)

	encodedHash := fmt.Sprintf("$argon2id$v=%d$m=%d,t=%d,p=%d$%s$%s", argon2.Version, s.params.Memory, s.params.Iterations, s.params.Parallelism, b64Salt, b64Hash)

	return encodedHash, nil
}

// ComparePasswordAndHash Compares the given `password` and `encodedHash`.
func (s *Argon2HashingStrategy) ComparePasswordAndHash(password string, encodedHash string) (bool, error) {
	params, salt, hash, err := s.decodeHash(encodedHash)
	if err != nil {
		return false, err
	}

	otherHash := argon2.IDKey([]byte(password), salt, params.Iterations, params.Memory, params.Parallelism, params.KeyLength)

	if subtle.ConstantTimeCompare(hash, otherHash) == 1 {
		return true, nil
	}
	return false, nil
}

func (s *Argon2HashingStrategy) decodeHash(encodedHash string) (params *Argon2HashingParams, salt []byte, hash []byte, err error) {
	values := strings.Split(encodedHash, "$")
	if len(values) != 6 {
		return nil, nil, nil, fmt.Errorf("the encoded hash is not in the correct format")
	}

	var version int
	_, err = fmt.Sscanf(values[2], "v=%d", &version)
	if err != nil {
		return nil, nil, nil, err
	}
	if version != argon2.Version {
		return nil, nil, nil, fmt.Errorf("incompatible version of argon2")
	}

	params = &Argon2HashingParams{}
	_, err = fmt.Sscanf(values[3], "m=%d,t=%d,p=%d", &params.Memory, &params.Iterations, &params.Parallelism)
	if err != nil {
		return nil, nil, nil, err
	}

	salt, err = base64.RawStdEncoding.DecodeString(values[4])
	if err != nil {
		return nil, nil, nil, err
	}
	params.SaltLength = uint32(len(salt))

	hash, err = base64.RawStdEncoding.DecodeString(values[5])
	if err != nil {
		return nil, nil, nil, err
	}
	params.KeyLength = uint32(len(hash))

	return params, salt, hash, nil
}

func generateRandomBytes(n uint32) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}
