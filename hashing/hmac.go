package hashing

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"fmt"
)

type HMAC struct {
}

func (h *HMAC) CheckMAC(payload []byte, hash []byte, secret []byte) bool {
	mac := hmac.New(sha512.New, secret)
	mac.Write(payload)
	expected := mac.Sum(nil)

	return hmac.Equal(hash, expected)
}

func (h *HMAC) SignHMAC(payload []byte, secret []byte) []byte {
	mac := hmac.New(sha512.New, secret)
	mac.Write(payload)
	signed := mac.Sum(nil)
	return signed
}

func (h *HMAC) ValidateHMAC(payload []byte, hash string, secret string) error {
	var validated error

	if len(hash) > 7 {
		hashingMethod := hash[:7]
		if hashingMethod != "sha512=" {
			return fmt.Errorf("unexpected hashing method: %s", hashingMethod)
		}

		messageMAC := hash[7:] // first few chars are: sha1=
		messageMACBuf, _ := hex.DecodeString(messageMAC)

		res := h.CheckMAC(payload, []byte(messageMACBuf), []byte(secret))
		if res == false {
			validated = fmt.Errorf("invalid message digest or secret")
		}
	} else {
		return fmt.Errorf("invalid encodedHash, should have at least 5 characters")
	}

	return validated
}
