package consumer

type Consumer interface {
	Consume(data []byte) error
	GetExchange() string
	GetQueue() string
	GetRoutingKey() string
	GetTag() string
	GetErrorChannel() chan error
}
