package consumer

import (
	"fmt"
	"log"
	"time"

	"github.com/streadway/amqp"
	"gitlab.com/robottle/robottle_common/connection"

	"gitlab.com/robottle/robottle_common/config"
)

type AMQPConsumerHandler struct {
	settings             *config.Messaging
	conn                 *amqp.Connection
	channel              *amqp.Channel
	consumersChannel     chan Consumer
	consumers            map[string]Consumer
	connectionCloseError chan *amqp.Error
}

func NewAMQPConsumerHandler(settings *config.Messaging) (Handler, error) {
	handler := &AMQPConsumerHandler{
		settings:             settings,
		consumersChannel:     make(chan Consumer),
		consumers:            make(map[string]Consumer),
		connectionCloseError: make(chan *amqp.Error),
	}
	return handler, nil
}

func (h *AMQPConsumerHandler) Start() (err error) {
	if err = h.connect(); err != nil {
		return
	}
	defer func() {
		_ = h.conn.Close()
		_ = h.channel.Close()
	}()

	go func() {
		for {
			if err := <-h.connectionCloseError; err != nil {
				_ = h.connect()
			}
		}
	}()

	err = h.listen()
	h.connectionCloseError <- amqp.ErrClosed
	return
}

func (h *AMQPConsumerHandler) AddConsumer(consumer Consumer) (err error) {
	if h.conn == nil {
		if err = h.connect(); err != nil {
			return
		}
	}
	if h.channel == nil {
		if err = h.createChannel(); err != nil {
			return
		}
	}
	tag := consumer.GetTag()
	if _, exists := h.consumers[tag]; exists {
		log.Printf("Consumer %s already exists", tag)
		return nil
	}

	exchange := consumer.GetExchange()
	queue := consumer.GetQueue()
	routingKey := consumer.GetRoutingKey()
	log.Printf("Adding consumer %s for exchange %s on queue %s with routing key %s",
		tag, exchange, queue, routingKey)
	if err = h.declareExchange(exchange, "topic"); err != nil {
		return fmt.Errorf("error declaring exchange %s: %v", exchange, err)
	}
	if err = h.declareQueue(queue); err != nil {
		return fmt.Errorf("error declaring queue %s: %v", queue, err)
	}
	if err = h.bindQueue(queue, exchange, routingKey); err != nil {
		return fmt.Errorf("error binding queue %s to exchange %s with routing key %s: %v",
			queue, exchange, routingKey, err)
	}
	//if err = h.channel.Qos(1, 0, false); err != nil {
	//	return fmt.Errorf("error setting QOS: %v", err)
	//}
	h.consumersChannel <- consumer
	return
}

func (h *AMQPConsumerHandler) declareExchange(exchangeName string, exchangeType string) (err error) {
	err = h.channel.ExchangeDeclare(exchangeName, exchangeType, true, false, false, false, nil)
	return
}

func (h *AMQPConsumerHandler) declareQueue(queueName string) (err error) {
	_, err = h.channel.QueueDeclare(queueName, true, false, false, false, nil)
	return
}

func (h *AMQPConsumerHandler) bindQueue(queueName string, exchangeName string, routingKey string) (err error) {
	err = h.channel.QueueBind(queueName, routingKey, exchangeName, false, nil)
	return
}

func (h *AMQPConsumerHandler) connect() (err error) {
	for {
		h.conn, err = connection.GetAMQPConnection(h.settings)
		if err == nil {
			h.conn.NotifyClose(h.connectionCloseError)
			if err = h.createChannel(); err != nil {
				return
			}
			return
		}
		time.Sleep(1 * time.Second)
	}
}

func (h *AMQPConsumerHandler) createChannel() (err error) {
	h.channel, err = h.conn.Channel()
	return
}

func (h *AMQPConsumerHandler) listen() error {
	for consumer := range h.consumersChannel {
		queue := consumer.GetQueue()
		tag := consumer.GetTag()
		deliveries, err := h.channel.Consume(
			queue,
			tag,
			false,
			false,
			false,
			false,
			nil,
		)
		if err != nil {
			log.Printf(
				"error declaring consumer %s for queue %s: %v",
				tag,
				queue,
				err,
			)
			continue
		}

		go h.handle(consumer, deliveries)

		h.consumers[tag] = consumer
	}
	return nil
}

func (h *AMQPConsumerHandler) handle(consumer Consumer, deliveries <-chan amqp.Delivery) {

	go func(consumer Consumer) {
		errorChannel := consumer.GetErrorChannel()
		for err := range errorChannel {
			if err != nil {
				log.Printf("error consuming data: %v", err)
			}
		}
	}(consumer)
	for delivery := range deliveries {
		go func(delivery amqp.Delivery, consumer Consumer) {
			errorChannel := consumer.GetErrorChannel()
			if err := consumer.Consume(delivery.Body); err != nil {
				log.Printf("error consuming delivery: %v", err)
				if err := delivery.Reject(true); err != nil {
					log.Printf("error rejecting delivery: %v", err)
					errorChannel <- err
				}
				return
			}
			if err := delivery.Ack(false); err != nil {
				log.Printf("error acknowledgin delivery: %v", err)
				errorChannel <- err
			}
		}(delivery, consumer)
	}
}
