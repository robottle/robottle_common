package consumer

import (
	"fmt"

	"gitlab.com/robottle/robottle_common/config"
)

type Handler interface {
	Start() error
	AddConsumer(consumer Consumer) error
	// ConsumerForTag(tag string) (consumer Consumer, found bool)
	// ConsumerTag(parts ...string) string
	// Consumers() map[string]Consumer
}

func NewConsumerHandler(settings *config.Messaging) (Handler, error) {
	switch settings.Adapter {
	case config.AMQPMessagingAdapter:
		return NewAMQPConsumerHandler(settings)
	default:
		return nil, fmt.Errorf("messaging adapter not supported: %s", settings.Adapter)
	}
}
