module gitlab.com/robottle/robottle_common

go 1.12

require (
	github.com/armon/circbuf v0.0.0-20190214190532-5111143e8da2 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/jinzhu/configor v1.0.0
	github.com/jinzhu/gorm v1.9.5
	github.com/kr/pty v1.1.3 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/micro/go-micro v1.1.0
	github.com/streadway/amqp v0.0.0-20190312223743-14f78b41ce6d
	github.com/stretchr/testify v1.3.0
	github.com/thoas/go-funk v0.4.0
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.0.1
	golang.org/x/crypto v0.0.0-20190426145343-a29dc8fdc734
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/rethinkdb/rethinkdb-go.v5 v5.0.1
	grpc.go4.org v0.0.0-20170609214715-11d0a25b4919 // indirect
)
