package wrapper

import (
	"context"
	"fmt"

	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	"github.com/micro/go-micro/registry"
	"github.com/micro/go-micro/server"

	"gitlab.com/robottle/robottle_common/auth"
)

const (
	methodKey           = "Micro-Method"
	authorizationHeader = "Authorization"
)

// NewAuthCallWrapper returns a call wrapper that injects an authorization
// token to the context.
func NewAuthCallWrapper() client.CallWrapper {
	return func(cf client.CallFunc) client.CallFunc {
		return func(ctx context.Context, node *registry.Node, req client.Request, res interface{}, opts client.CallOptions) error {
			md, ok := metadata.FromContext(ctx)
			if !ok {
				md = make(map[string]string)
			}
			if token := ctx.Value("token"); token != nil {
				md[authorizationHeader] = token.(string)
			}
			ctx = metadata.NewContext(ctx, md)
			return cf(ctx, node, req, res, opts)
		}
	}
}

// NewAuthHandlerWrapper returns a handler wrapper that checks for a token
// and validates it.
func NewAuthHandlerWrapper(authStrategy auth.Strategy, allowedMethods []string) server.HandlerWrapper {
	return func(h server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, res interface{}) error {
			md, ok := metadata.FromContext(ctx)
			if !ok {
				md = make(map[string]string)
			}
			md = metadata.Copy(md)
			method := md[methodKey]
			if !isAllowed(method, allowedMethods) {
				token, found := md[authorizationHeader]
				if !found {
					return fmt.Errorf("authorization token not found")
				}
				data, err := authStrategy.VerifyToken(token)
				if err != nil {
					return fmt.Errorf("invalid token: %v", err)
				}
				for key, value := range data {
					md[key] = fmt.Sprintf("%v", value)
				}
			}
			return h(ctx, req, res)
		}
	}
}

func isAllowed(method string, allowedMethods []string) bool {
	for _, allowed := range allowedMethods {
		if method == allowed {
			return true
		}
	}
	return false
}
