package publisher

import (
	"fmt"

	"gitlab.com/robottle/robottle_common/config"
)

type Publisher interface {
	Publish(consumerInfo *config.Consumer, body []byte) error
}

func NewPublisher(settings *config.Messaging) (Publisher, error) {
	switch settings.Adapter {
	case config.AMQPMessagingAdapter:
		return NewAMQPPublisher(settings)
	default:
		return nil, fmt.Errorf("messaging adapter not supported: %s", settings.Adapter)
	}
}
