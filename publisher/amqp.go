package publisher

import (
	"fmt"

	"github.com/streadway/amqp"
	"gitlab.com/robottle/robottle_common/config"
	"gitlab.com/robottle/robottle_common/connection"
)

type AMQPPublisher struct {
	settings *config.Messaging
}

func NewAMQPPublisher(settings *config.Messaging) (Publisher, error) {
	publisher := &AMQPPublisher{
		settings: settings,
	}
	return publisher, nil
}

func (p *AMQPPublisher) Publish(consumerInfo *config.Consumer, body []byte) (err error) {
	conn, err := connection.GetAMQPConnection(p.settings)
	if err != nil {
		return err
	}
	defer func() {
		_ = conn.Close()
	}()
	channel, err := conn.Channel()
	if err != nil {
		return err
	}
	defer func() {
		_ = channel.Close()
	}()
	publishing := amqp.Publishing{
		ContentType:  "text/plain",
		Body:         body,
		DeliveryMode: amqp.Persistent,
	}
	err = channel.Publish(
		consumerInfo.Exchange,
		consumerInfo.RoutingKey,
		false,
		false,
		publishing,
	)
	if err != nil {
		err = fmt.Errorf(
			"error publishing message to exchange %s with routing key %s: %v",
			consumerInfo.Exchange,
			consumerInfo.RoutingKey,
			err,
		)
	}
	return
}
