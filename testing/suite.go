package testing

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/stretchr/testify/suite"

	cfg "gitlab.com/robottle/robottle_common/config"
	"gitlab.com/robottle/robottle_common/connection"
)

// Suite is a custom testing suite.
type Suite struct {
	suite.Suite

	Config *cfg.Config

	wd string
}

// Init initializes the suite.
func (suite *Suite) Init() {
	r := suite.Require()

	wd, err := os.Getwd()
	r.Nil(err)

	parent := filepath.Dir(wd)
	configFile := strings.Join([]string{parent, "config.yml"}, string(os.PathSeparator))

	err = cfg.SetConfigurationFile(configFile)
	r.Nil(err)

	config, err := cfg.GetConfig()
	r.Nil(err)

	suite.Config = config
	suite.wd = wd
}

// DeleteDatabaseFile deletes the database for the given `connectionName`.
// This is intended to be used for SQLite database connections.
func (suite *Suite) DeleteDatabaseFile(connectionName string) {
	r := suite.Require()

	settings, found := suite.Config.Database[connectionName]
	r.Truef(found, "database settings not found for %s", connectionName)

	databaseFile := strings.Join([]string{suite.wd, settings.Database}, string(os.PathSeparator))
	if _, err := os.Stat(databaseFile); os.IsNotExist(err) {
		return
	}
	err := os.Remove(databaseFile)
	r.Nilf(err, "error removing database file: %v", err)
}

// TruncateTables empties the given database tables for the given `connectionName`.
func (suite *Suite) TruncateTables(connectionName string, tableNames ...string) {
	r := suite.Require()

	settings, found := suite.Config.Database[connectionName]
	r.Truef(found, "connection settings not found for %s", connectionName)

	db, err := connection.GetDatabaseConnection(connectionName)
	r.Nilf(err, "error getting database connection for %s: %v", connectionName, err)

	sql := "TRUNCATE TABLE %s"
	if settings.Adapter == cfg.SQLiteDatabaseAdapter {
		sql = "DELETE FROM %s"
	}
	for _, tableName := range tableNames {
		statement := fmt.Sprintf(sql, tableName)
		_, err := db.DB().Exec(statement)
		r.Nilf(err, "error truncating table %s: %v", tableName, err)
	}
}

// MigrateModels migrates the given `models` for the database with the given `connectionName`.
func (suite *Suite) MigrateModels(connectionName string, models ...interface{}) {
	r := suite.Require()

	db, err := connection.GetDatabaseConnection(connectionName)
	r.Nilf(err, "error getting database connection for %s: %v", connectionName, err)

	err = db.AutoMigrate(models...).Error
	r.Nilf(err, "error migrating models: %v", err)
}
